$(document).ready(function () {
  $("body").on("load", function () {
    $.ajax({
      type: "GET",
      url: "https://jsonplaceholder.typicode.com/posts",
      dataType: "json",
      async: true,
      success: function (data) {
        console.log(data);
        let products = "";
        $.each(data, function (i, v) {
          products += `
          <div class="product">
            <h4>${v.userId}</h4>
            <h3>${v.id}</h3>
            <h2>${v.title.substring(0, 10)}</h2>
            <p>${v.body.substring(0, 10)} </p>
            <button>add to cart</button>
          </div>
          `;
        });
        $(".product-container").append(products);
        $("#waiting").hide();
      },
      error: function () {
        console.log("not able to process request");
      },
    });
  });



  
  $("body").trigger("load");

  $("#searchkey").on("keyup", function () {
    let key = $(this).val().toLowerCase();

    $(".product").filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(key) > -1);
    });
  });
});
